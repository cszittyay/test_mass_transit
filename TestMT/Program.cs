﻿using MassTransit;
using System;
using TestMT;

namespace RabbitTest
{
    public class Program
    {
        public static void Main()

        {
            // Crear el 'Bus'
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri(RabbitMqConstants.RabbitMqUri), h =>
                {
                    h.Username(RabbitMqConstants.UserName);
                    h.Password(RabbitMqConstants.Password);
                });


              
            });

            // Se inicia el servicio de transporte
            bus.Start();

            for (int i = 1; i < 100; i++)
            {
                // Se publica un mensaje
                bus.Publish(new YourMessage { Text = $"Hi", Id = i });
                if(i%5==0) bus.Publish(new OtroMensaje { Nota = $"Una nota..-", Id = i });

                Console.Write('.');

                System.Threading.Thread.Sleep(100);
            }

            
            bus.Stop();
        }



    }

    public class YourMessage {

        public string Text { get; set; }
        public int Id { get; set; }
    }

    public class OtroMensaje
    {

        public string Nota { get; set; }
        public int Id { get; set; }
    }
}