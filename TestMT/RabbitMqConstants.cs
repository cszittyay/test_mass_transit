﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMT
{
    static public class RabbitMqConstants
    {
        public const string RabbitMqUri = "rabbitmq://localhost/degas/";
        public const string UserName = "guest";
        public const string Password = "guest";
        public const string AperturaNtc = "apertura_ntc";
    }
}
