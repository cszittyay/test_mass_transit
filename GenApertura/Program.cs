﻿using MassTransit;
using RabbitTest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMT;

namespace GenApertura
{
    class Program
    {
        public static void Main()

        {
            // Crear el 'Bus'
            Console.WriteLine("Creando el bus");
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                var host = sbc.Host(new Uri(RabbitMqConstants.RabbitMqUri), h =>
                {
                    h.Username(RabbitMqConstants.UserName);
                    h.Password(RabbitMqConstants.Password);
                });

                

                //// Indicar el EndPoint: en este caso AperturaNtc
                sbc.ReceiveEndpoint(host, RabbitMqConstants.AperturaNtc, ep =>
                {
                    
                    // Este es el método que se va a ejecutar cuando  ser reciba el mensaje.
                    ep.Handler<YourMessage>(context =>
                    {
                        System.Threading.Thread.Sleep(100);
                        return Console.Out.WriteLineAsync($"Received (1000): {context.Message.Text} --> {context.Message.Id,5} ");
                    });


                    ep.Handler<OtroMensaje>(context =>
                    {
                        System.Threading.Thread.Sleep(100);
                        return Console.Out.WriteLineAsync($"Received (1000): {context.Message.Nota} --> {context.Message.Id,5} ");
                    });
                });


              

            });

            bus.Start();
            Console.WriteLine("Esperando mensajes....");
            Console.ReadKey();


        }
    }
}
